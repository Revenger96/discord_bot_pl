/**
 * Copyright (c) 2019
 *
 * @summary Project SilverLake Bot
 * @author Revenger96 <malthethorhaugehansen@gmail.com>
 *
 * Created at     : 2019-16-04 01:48:00
 * Last modified  : 2019-19-04 21:20:00
 */
// Setup gameDig
const Gamedig = require('gamedig');
// Setup of Discord dependencies
const Discord = require('discord.js')
const client = new Discord.Client()
client.login('NTY3MTIyMjg4MTI2ODUzMTQy.XLTg3g.GJbWzVuyGAXKLyNQOi3675Zo2Cg')
const prefix = '!'
// Moment dependencies to measure time until restart
var moment = require('moment')
moment().format()
var fs = require('fs')
var scriptStarted
var adminLogOld = null
var surveilanceLogOld = null
// Creates a console log if the bot is working.
client.on('ready', () => {
  console.log('Project Silverlake Discord Bot - Online')
  console.log('Created by Revenger96')
  // client.user.setGame('Project-Silverlake.com')
  client.user.setActivity('Project-Silverlake.com', { type: 'WATCHING' })
  scriptStarted = false
})

/**
 * Function that takes not of current time and calculates the difference.
 * Uses Moment.js to get current server time and difference.
 */
function getRestartTimes() {
  // Get current server time.
  var currentTime = moment()
  // Sets the timezones, reconfigure compared to what timezone the box is running in.
  var serverRestart1 = currentTime.clone().hour(11).minute(0).second(0)
  var timeLeft1 = serverRestart1.diff(currentTime)
  var serverRestart2 = currentTime.clone().hour(15).minute(0).second(0)
  var timeLeft2 = serverRestart2.diff(currentTime)
  var serverRestart3 = currentTime.clone().hour(19).minute(0).second(0)
  var timeLeft3 = serverRestart3.diff(currentTime)
  var serverRestart4 = currentTime.clone().hour(23).minute(0).second(0)
  var timeLeft4 = serverRestart4.diff(currentTime)
  var nextDay = moment(currentTime).add(4, 'hours')
  var serverRestart5 = nextDay.clone().hour(3).minute(0).second(0)
  var timeLeft5 = serverRestart5.diff(currentTime)
  var serverRestart6 = currentTime.clone().hour(7).minute(0).second(0)
  var timeLeft6 = serverRestart6.diff(currentTime)
  var untilRestart = [timeLeft1, timeLeft2, timeLeft3, timeLeft4, timeLeft5, timeLeft6]
  var fHours, fMinutes, fSeconds
  function goodTime(time) {
    if (time < 14400000 && time > 0) {
      fHours = moment.utc(time).format('HH')
      fMinutes = moment.utc(time).format('mm')
      fSeconds = moment.utc(time).format('ss')
    }
  }
  untilRestart.forEach(goodTime)
  // Server info
  let serverRestart = new Discord.RichEmbed()
    .setColor(3447003)
    .setThumbnail('https://i.imgur.com/MoBtlpr.png')
    .addField('**Server Information:**', 'Map: ' + currentMap + '\n' + 'Players: ' + currentPlayers.length + ' / ' + maxPlayers)
    .addField('**The ARMA 3 server will restart in:**', fHours + '** Hours **' + fMinutes + '** Minutes **' + fSeconds + '** Seconds **')
    .setTimestamp()
  return serverRestart
};
getServerDetails()
client.on('message', async (message) => {
  if (!message.content.startsWith(prefix)) return
  let unformatted = message.content.split(' ')[0]
  unformatted = unformatted.slice(prefix.length)
  let cmd = unformatted.toLowerCase()
  let args = message.content.split(' ').slice(1)

  if (message.channel.name === 'bot-commands') {
    if (cmd === 'help' || cmd === 'info') {
      let helpCMD = new Discord.RichEmbed()
        .setColor(3447003)
        .setThumbnail('https://i.imgur.com/MoBtlpr.png')
        .addField('**Commands:**', '`!help or !info` *Displays all available commands.*\n`!changelog` *Displays the changelog of the discord bot*\n`!restart` *Displays next restart.*\n`!repo` *Displays repo information.*\n`!howto` *Displays howto tutorials. Type `!howto commands` for more info.*')
        .addBlankField(true)
        .addField('**Administrator Commands:**', '`!announcement title | body` *Posts an message to announcements.*\n`!event title | body` *Posts an message to events.*')
      message.channel.send(helpCMD)
    }
    if (cmd === '!restartlogs') {
      if (message.member.roles.find(n => n.name === 'Developer')) {
        message.channel.send('Old logs have been reset to empty, wait for the bot to check logs again.')
        adminLogOld = null
        surveilanceLogOld = null
      } else {
        message.channel.send("Sorry you don't have permission to use this command. Requires the Developer tag.")
      }
    }
    // Howto function
    if (cmd === 'howto') {
      let commandHowtoU = args.join()
      let commandHowto = commandHowtoU.toLowerCase()
      let howtoCMD = new Discord.RichEmbed()
        .setColor(3447003)
        .setThumbnail('https://i.imgur.com/MoBtlpr.png')
      if (commandHowto === '') {
        message.channel.send('Please specify what you need help with. For a list of all commands type: `!howto commands`')
      }
      if (commandHowto === 'commands') {
        howtoCMD.addField('Available HowTo commands:', ' - `!howto join` Displays howto join the server.\n - `!howto mods` Displays howto install the mods.\n - `!howto apply` Displays howto apply for the server.\n - `!howto tfr` Displays howto install task force radio.\n')
        message.channel.send(howtoCMD)
      }
      if (commandHowto === 'join') {
        howtoCMD.addField('HowTo join the server:', '1. Download the mod repo. Type `!howto mods` for more info.\n2. Open up the Arma 3 launcher.\n3. Select `Mods` in the most right selection bar.\n4. Hit `Local Mod` in the top and find where the addon was installed with Arma 3 Sync.\n5. Hit `Play`, you should now launch the game with our addons!')
        message.channel.send(howtoCMD)
      }
      if (commandHowto === 'mods') {
        howtoCMD.addField('HowTo download the mods:', 'Please follow this excelent tutorial on how you would install our mod pack:\nhttps://www.youtube.com/watch?v=7wz-uFldXwQ')
        message.channel.send(howtoCMD)
      }
      if (commandHowto === 'apply') {
        howtoCMD.addField('HowTo apply for the server:', '1. Visit https://project-silverlake.com/\n2. Create an account, remember to use your RP name! Fx: `Chris Hamilton`\n3. Head over to the application section. https://project-silverlake.com/forum/index.php?/application/\n4. Put effort into your application and post it. We will return to you as soon as possible with further instructions.')
        message.channel.send(howtoCMD)
      }
      if (commandHowto === 'tfr') {
        howtoCMD.addField('HowTo install Task Force Radio:', '1. Download Task Force Radio from: http://radio.task-force.ru/en/\n2. UnZip the file, and extract the contents of `@task_force_radio/teamspeak` to your desktop.\n3. Double click the file named: `task_force_radio.ts3_plugin`\n4. Restart Teamspeak.\n5. Head into `Tools --> Options` then `Addons` and enable the Task Force Radio plugin.')
        message.channel.send(howtoCMD)
      }
      if (commandHowto === 'hamilton') {
        howtoCMD.addField('Sorry...', '**you will never be as cool as Chris Hamilton!**')
        howtoCMD.setImage('https://i.imgur.com/4D7nY7u.png')
        message.channel.send(howtoCMD)
      }
    }
    if (cmd === 'restart') {
      message.channel.send(getRestartTimes())
    }

    if (cmd === 'repo') {
      let repoCMD = new Discord.RichEmbed()
        .setColor(3447003)
        .setThumbnail('https://i.imgur.com/MoBtlpr.png')
        .addField('**Repository Information:**', ':flag_us: **US:**  `ftp://18.218.42.7/.a3s/autoconfig`\n:flag_eu: **EU:** `ftp://54.194.106.11/.a3s/autoconfig`\n**Arma3Sync:** `www.armaholic.com/page.php?id=22199`')
      message.channel.send(repoCMD)
    }

    if (cmd === 'server') {
      let repoCMD = new Discord.RichEmbed()
        .setColor(3447003)
        .setThumbnail('https://i.imgur.com/MoBtlpr.png')
        .setTimestamp()
    }

    if (cmd === 'announcement') {
      if (message.member.roles.find(n => n.name === 'Announcer')) {
        const title = args.join(' ').split('|').slice(0, 1)
        const text = args.join(' ').split('|').slice(1)
        if (text.length < 1) {
          return message.channel.send("You can't send empty announcements.")
        }

        let announcementCMD = new Discord.RichEmbed()
          .setColor(3447003)
          .setThumbnail('https://i.imgur.com/MoBtlpr.png')
          .addField(title, text + '\n\n ( \@everyone )')
          .setFooter(message.author.username, message.author.avatarURL)
          .setTimestamp()
        message.guild.channels.find(c => c.name === 'announcements').send(announcementCMD).catch(console.error)
        message.channel.send('Your message has been posted.')
      } else {
        message.channel.send("Sorry you don't have permission to use this command. Requires the Announcer tag.")
      }
    }

    if (cmd === 'event') {
      if (message.member.roles.find(n => n.name === 'Announcer')) {
        const title = args.join(' ').split('|').slice(0, 1)
        const text = args.join(' ').split('|').slice(1)
        if (text.length < 1) {
          return message.channel.send("You can't send empty events.")
        }

        let announcementCMD = new Discord.RichEmbed()
          .setColor(3447003)
          .setThumbnail('https://i.imgur.com/MoBtlpr.png')
          .addField(title, text + '\n\n ( \@everyone )')
          .setFooter(message.author.username, message.author.avatarURL)
          .setTimestamp()
        message.guild.channels.find(c => c.name === 'events').send(announcementCMD).catch(console.error)
        message.channel.send('Your message has been posted.')
      } else {
        message.channel.send("Sorry you don't have permission to use this command. Requires the Announcer tag.")
      }
    }

    if(cmd === 'servers') {
      let serverCmd = new Discord.RichEmbed()
      .setColor(3447003)
      .setThumbnail('https://i.imgur.com/MoBtlpr.png')
      .addField('Arma 3 Server:', '**Server Name: **' + armaCurrentName + "\n" + '**Map: **' + armaCurrentMap + "\n" + '**Current Players: **' + armaCurrentPlayers + " / " + armaMaxPlayers)
      message.channel.send(serverCmd)
    }

    if (cmd === 'changelog') {
      let changelogCMD = new Discord.RichEmbed()
        .setColor(3447003)
        .setThumbnail('https://i.imgur.com/MoBtlpr.png')
        .addField('Version: 1.1', ' - The bot now accepts any type of lowercase/uppercase composition.\n - Added the command `!info` to the bot to ease confusion.\n - The bot now automatically posts logs from the server.\n - The bot now accepts `!event` commands.\n - The bot now accepts `!howto` commands.')
      message.channel.send(changelogCMD)
    }
  } else {
    message.channel.send('Please post all bot commands in #bot-commands')
  }
  /*
  if (scriptStarted === false) {
    scriptStarted = true
    setInterval(() => {
      console.log('Checking and posting logs')
      // Read new logs
      var adminLogNewU = fs.readFileSync('test_directory/admin.log', 'utf-8')
      var surveilanceLogNewU = fs.readFileSync('test_directory/surveilance.log', 'utf-8')
      // Split new logs into arrays
      var adminLogNew = adminLogNewU.split('\n')
      var surveilanceLogNew = surveilanceLogNewU.split('\n')

      // Check if the old logs are empty
      if (!Array.isArray(adminLogOld)) {
        adminLogOld = adminLogNew
      }
      if (!Array.isArray(surveilanceLogOld)) {
        surveilanceLogOld = surveilanceLogNew
      }
      // Check and add any differences in the logs to the embed.
      if (adminLogNew.length > adminLogOld.length || surveilanceLogNew.length > surveilanceLogOld.length) {
        if (adminLogNew.length > adminLogOld.length) {
          let differenceA = adminLogNew.filter(x => !adminLogOld.includes(x))
          differenceA.join('\n')
          adminLogOld = adminLogNew
          if (!differenceA === '') {
            let discordLog = new Discord.RichEmbed()
              .setColor(3447003)
              .setThumbnail('https://i.imgur.com/MoBtlpr.png')
              .setTimestamp()
              .addField('Admin Logs:', differenceA)

            message.guild.channels.find(c => c.name === 'server-logs').send(discordLog).catch(console.error)
          }
        }
        if (surveilanceLogNew.length > surveilanceLogOld.length) {
          let differenceS = surveilanceLogNew.filter(x => !surveilanceLogOld.includes(x))
          differenceS.join('\n')
          surveilanceLogOld = surveilanceLogNew
          if (!differenceS === '') {
            let discordLog = new Discord.RichEmbed()
              .setColor(3447003)
              .setThumbnail('https://i.imgur.com/MoBtlpr.png')
              .setTimestamp()
              .addField('Surveilance Logs:', differenceS)
            message.guild.channels.find(c => c.name === 'server-logs').send(discordLog).catch(console.error)
          }
        }
      }
    }, 300000)
  }*/
})

// Holds information about the servers.

// Arma 3:
let armaCurrentPlayers
let armaMaxPlayers
let armaCurrentMap
let armaCurrentName

// Teamspeak 3

function getServerDetails () {
  // Arma Query:
  Gamedig.query({
    type: 'arma3',
    host: '142.44.143.13'
  }).then((state) => {
    // Set arma server values:
    armaCurrentMap = Object.values(state)[1]
    armaCurrentName = Object.values(state)[0]
    armaMaxPlayers = Object.values(state)[4]
    let armaPlayers = Object.values(state)[5]
    armaCurrentPlayers = armaPlayers.length
  }).catch((error) => {
    console.log("Arma server is offline");
  });
  // Teamspeak Query:
  Gamedig.query({
    type: 'teamspeak3',
    host: 'ts3.project-silverlake.com'
  }).then((state) => {
    // Set arma server values:
    console.log(Object.values(state)[0])
  }).catch((error) => {
    console.log("Teamspeak server is offline");
  });
}

// Calls the function once on startup, and then every 5 minutes.
getServerDetails()

setInterval (() => {
  getServerDetails()
}, 300000)