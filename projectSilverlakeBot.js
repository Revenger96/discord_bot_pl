/**
 * Copyright (c) 2019
 *
 * @summary Project SilverLake Bot
 * @author Revenger96 <malthethorhaugehansen@gmail.com>
 *
 * @version 1.0.3
 */

// Requirements
const Discord = require('discord.js')
const Gamedig = require('gamedig')
var request = require('request')
var moment = require('moment')
moment().format()
var fs = require('fs')

// Discord Login & Client
const client = new Discord.Client()
client.login('NTY3MTIyMjg4MTI2ODUzMTQy.XNNrUw.QGCrOFPcs7KuxuttiNEUcfEQ3O8')
const prefix = '!'

// Stores logs of the server
var adminLogOld = null
var surveilanceLogOld = null
let adminLogNew = null
let surveilanceLogNew = null
let startupCheck = true
// Stores information about the Arma and Teamspeak server.
// var teamspeakOnline
var PLarmaOnline

var PLarmaCurrentPlayers
var PLarmaMaxPlayers
var PLarmaCurrentMap

var MarmaOnline

var MarmaCurrentPlayers
var MarmaMaxPlayers
var MarmaCurrentMap
// var armaCurrentName

// On bot startup.
client.on('ready', () => {
  console.log('Roleplay Network Discord Bot - Online')
  console.log('Version: 1.0.4 - Created by Revenger96')
  // client.user.setGame('Project-Silverlake.com')
  client.user.setActivity('Server stats updating...', { type: 'WATCHING' })
})
// Aquires server information on the Arma and Teamspeak server.,
function getServerStatus () {
  // Arma Query:
  Gamedig.query({
    type: 'arma3',
    host: '142.44.143.13'
  }).then((state) => {
    // Set arma server values:
    PLarmaOnline = true
    PLarmaCurrentMap = Object.values(state)[1]
    PLarmaMaxPlayers = Object.values(state)[4]
    let armaPlayers = Object.values(state)[5]
    PLarmaCurrentPlayers = armaPlayers.length
  }).catch((error) => {
    PLarmaOnline = false
    console.log(error)
  })
  // Teamspeak Query:
  /**
  Gamedig.query({
      type: 'teamspeak3',
      host: '142.44.143.13',
      port: '9987',
      debug: 'true'
  }).then((state) => {
      teamspeakOnline = true
  }).catch((error) => {
      teamspeakOnline = false
      console.log('Server offline.')
  }); */
}
// Calls the function once on startup, and then every 5 minutes.
getServerStatus()
setInterval(() => {
  getServerStatus()
}, 300000)
// Gets server details and returns the embed.
function getServerDetails (message) {
  // Resets the embed between uses.
  var serverEmbed = new Discord.RichEmbed()
    .setColor(3447003)
    .setThumbnail('https://i.imgur.com/7hKPqeU.png')
    .setTimestamp()
  // Gets current time of the server box.
  let currentTime = moment()
  // Saves the restart times in comparison to the already saved time, and saves the difference between them.
  let serverRestart1 = currentTime.clone().hour(11).minute(0).second(0)
  let timeLeft1 = serverRestart1.diff(currentTime)
  let serverRestart2 = currentTime.clone().hour(15).minute(0).second(0)
  let timeLeft2 = serverRestart2.diff(currentTime)
  let serverRestart3 = currentTime.clone().hour(19).minute(0).second(0)
  let timeLeft3 = serverRestart3.diff(currentTime)
  let serverRestart4 = currentTime.clone().hour(23).minute(0).second(0)
  let timeLeft4 = serverRestart4.diff(currentTime)
  let nextDay = moment(currentTime).add(4, 'hours')
  let serverRestart5 = nextDay.clone().hour(3).minute(0).second(0)
  let timeLeft5 = serverRestart5.diff(currentTime)
  let serverRestart6 = currentTime.clone().hour(7).minute(0).second(0)
  let timeLeft6 = serverRestart6.diff(currentTime)
  // Adds the differences to an array.
  let untilRestart = [timeLeft1, timeLeft2, timeLeft3, timeLeft4, timeLeft5, timeLeft6]
  // Stores split information about the restart.
  let fHours, fMinutes, fSeconds
  // Loops through the array and checks which restart is relevant, and saves them to the split information.
  function goodTime (time) {
    if (time < 14400000 && time > 0) {
      fHours = moment.utc(time).format('HH')
      fMinutes = moment.utc(time).format('mm')
      fSeconds = moment.utc(time).format('ss')
    }
  }
  untilRestart.forEach(goodTime)
  if (PLarmaOnline) {
    serverEmbed.addField('**Project Silverlake status:**', 'Project Silverlake: Online :white_check_mark:\n' + 'Map: ' + PLarmaCurrentMap + '\n' + 'Players: ' + PLarmaCurrentPlayers + ' / ' + PLarmaMaxPlayers)
  } else {
    serverEmbed.addField('**Project Silverlake  status:**', 'Project Silverlake: Offline :octagonal_sign:')
  }
  if (MarmaOnline) {
    serverEmbed.addField('**Project Metro status:**', 'Project Metro: Online :white_check_mark:\n' + 'Map: ' + MarmaCurrentMap + '\n' + 'Players: ' + MarmaCurrentPlayers + ' / ' + MarmaMaxPlayers)
  } else {
    serverEmbed.addField('**Project Metro status:**', 'Project Metro: Offline :octagonal_sign:')
  }
  serverEmbed.addField('**The servers will restart in:**', fHours + '** Hours **' + fMinutes + '** Minutes **' + fSeconds + '** Seconds **')
  serverEmbed.setFooter('!restart / !servers')
  return message.channel.send(serverEmbed)
}
// Posts an announcement
function postAnnouncement (args, message) {
  // Resets the embed between uses.
  var announcementEmbed = new Discord.RichEmbed()
    .setColor(3447003)
    .setThumbnail('https://i.imgur.com/7hKPqeU.png')
    .setTimestamp()
  if (message.member.roles.find(n => n.name === 'Announcer')) {
    const title = args.join(' ').split('|').slice(0, 1)
    const text = args.join(' ').split('|').slice(1)
    if (text.length < 1) {
      return message.channel.send("You can't send empty announcements.")
    }
    announcementEmbed.addField(title, text + '\n\n ( \@everyone )')
    announcementEmbed.setFooter(message.author.username, message.author.avatarURL)
    message.guild.channels.find(c => c.name === 'announcements').send(announcementEmbed).catch(console.error)
  } else {
    message.channel.send("Sorry you don't have permission to use this command. Requires the Announcer tag.")
  }
}
// Posts an Event notification.
function postEvent (args, message) {
  // Resets the embed between uses.
  var eventEmbed = new Discord.RichEmbed()
    .setColor(3447003)
    .setThumbnail('https://i.imgur.com/7hKPqeU.png')
    .setTimestamp()
  if (message.member.roles.find(n => n.name === 'Announcer')) {
    const title = args.join(' ').split('|').slice(0, 1)
    const text = args.join(' ').split('|').slice(1)
    if (text.length < 1) {
      return message.channel.send("You can't send empty events.")
    }
    eventEmbed.addField(title, text + '\n\n ( \@everyone )')
    eventEmbed.setFooter(message.author.username, message.author.avatarURL)
    message.guild.channels.find(c => c.name === 'events').send(eventEmbed).catch(console.error)
  } else {
    message.channel.send("Sorry you don't have permission to use this command. Requires the Announcer tag.")
  }
}
// Posts the changelog of the Discord bot.
function postChangelog (message) {
  // Resets the embed between uses.
  var changelogEmbed = new Discord.RichEmbed()
    .setColor(3447003)
    .setThumbnail('https://i.imgur.com/7hKPqeU.png')
    .setTimestamp()
    .addField('Version 1.2 *(Current)*', ' - Performance updates.\n - Bot now displays server status, type !servers to view.\n - The bot can now post polls, type !help for more info.\n - The bot now deletes your command message after you have posted it.')
    .addBlankField(true)
    .addField('Version 1.1', ' - The bot now accepts any type of lowercase/uppercase composition.\n - Added the command `!info` to the bot to ease confusion.\n - The bot now automatically posts logs from the server.\n - The bot now accepts `!event` commands.\n - The bot now accepts `!howto` commands.')
    .setFooter('!changelog')
  message.channel.send(changelogEmbed)
}
// Handles the howto messages.
function postHowto (message, args) {
  // Resets the embed between uses.
  var howtoEmbed = new Discord.RichEmbed()
    .setColor(3447003)
    .setThumbnail('https://i.imgur.com/7hKPqeU.png')
    .setTimestamp()
  var commandHowtoU = args.join()
  var commandHowto = commandHowtoU.toLowerCase()
  if (commandHowto === '') {
    return message.channel.send('Please specify what you need help with. For a list of all commands type: `!howto commands`')
  } else {
    if (commandHowto === 'commands') {
      howtoEmbed.addField('Available HowTo commands:', ' - `!howto join` Displays howto join the server.\n - `!howto mods` Displays howto install the mods.\n - `!howto apply` Displays howto apply for the server.\n - `!howto tfr` Displays howto install task force radio.\n')
      howtoEmbed.setFooter('!howto commands')
      return message.channel.send(howtoEmbed)
    }
    if (commandHowto === 'join') {
      howtoEmbed.addField('HowTo join the server:', '1. Download the mod repo. Type `!howto mods` for more info.\n2. Open up the Arma 3 launcher.\n3. Select `Mods` in the most right selection bar.\n4. Hit `Local Mod` in the top and find where the addon was installed with Arma 3 Sync.\n5. Hit `Play`, you should now launch the game with our addons!')
      howtoEmbed.setFooter('!howto join')
      return message.channel.send(howtoEmbed)
    }
    if (commandHowto === 'mods') {
      howtoEmbed.addField('HowTo download the mods:', 'Please follow this excelent tutorial on how you would install our mod pack:\nhttps://www.youtube.com/watch?v=7wz-uFldXwQ')
      howtoEmbed.setFooter('!howto mods')
      return message.channel.send(howtoEmbed)
    }
    if (commandHowto === 'apply') {
      howtoEmbed.addField('HowTo apply for the server:', '1. Visit https://project-silverlake.com/\n2. Create an account, remember to use your RP name! Fx: `Chris Hamilton`\n3. Head over to the application section. https://project-silverlake.com/forum/index.php?/application/\n4. Put effort into your application and post it. We will return to you as soon as possible with further instructions.')
      howtoEmbed.setFooter('!howto apply')
      return message.channel.send(howtoEmbed)
    }
    if (commandHowto === 'tfr') {
      howtoEmbed.addField('HowTo install Task Force Radio:', '1. Download Task Force Radio from: http://radio.task-force.ru/en/\n2. UnZip the file, and extract the contents of `@task_force_radio/teamspeak` to your desktop.\n3. Double click the file named: `task_force_radio.ts3_plugin`\n4. Restart Teamspeak.\n5. Head into `Tools --> Options` then `Addons` and enable the Task Force Radio plugin.')
      howtoEmbed.setFooter('!howto tfr')
      return message.channel.send(howtoEmbed)
    }
    if (commandHowto === 'hamilton') {
      howtoEmbed.addField('Sorry...', '**you will never be as cool as Chris Hamilton!**')
      howtoEmbed.setImage('https://i.imgur.com/4D7nY7u.png')
      howtoEmbed.setFooter('!howto hamilton')
      return message.channel.send(howtoEmbed)
    }
  }
}
// Posts the info or help message.
function postHelp (message) {
  // Resets the embed between uses.
  var helpEmbed = new Discord.RichEmbed()
    .setColor(3447003)
    .setThumbnail('https://i.imgur.com/7hKPqeU.png')
    .setTimestamp()
  helpEmbed.addField('**Commands:**', '`!help or !info` *Displays all available commands.*\n`!changelog` *Displays the changelog of the discord bot*\n`!restart` *Displays next restart.*\n`!repo` *Displays repo information.*\n`!howto` *Displays howto tutorials. Type `!howto commands` for more info.*\n`!suggest text` *Posts a suggestion to the suggestion channel. Replace text with your suggestion.*')
  helpEmbed.addBlankField(true)
  helpEmbed.addField('**Administrator Commands:**', '`!announcement title | body` *Posts an message to announcements.*\n`!event title | body` *Posts an message to events.*\n `!poll title | body` *Posts a poll to announcements*')
  helpEmbed.setFooter('!help / !info')
  return message.channel.send(helpEmbed)
}
function postPoll (message, args) {
  var pollEmbed = new Discord.RichEmbed()
    .setColor(3447003)
    .setThumbnail('https://i.imgur.com/7hKPqeU.png')
    .setTimestamp()
  if (message.member.roles.find(n => n.name === 'Announcer')) {
    const title = args.join(' ').split('|').slice(0, 1)
    const text = args.join(' ').split('|').slice(1)
    if (text.length < 1) {
      return message.channel.send("You can't send empty polls.")
    }
    pollEmbed.addField(title, text + '\n\n ( \@everyone )')
    pollEmbed.setFooter(message.author.username, message.author.avatarURL)
    message.guild.channels.find(c => c.name === 'announcements').send(pollEmbed).catch(console.error).then(async pollEmbed => {
      await pollEmbed.react('👍')
      await pollEmbed.react('👎')
    })
  } else {
    return message.channel.send("Sorry you don't have permission to use this command. Requires the Announcer tag.")
  }
}
function postSuggestion (message, args) {
  var suggestionEmbed = new Discord.RichEmbed()
    .setColor(3447003)
    .setThumbnail('https://i.imgur.com/7hKPqeU.png')
    .setTimestamp()
  const text = args.join(' ')
  console.log(text)
  if (text.length < 1) {
    return message.channel.send("You can't send empty suggestions.")
  }
  suggestionEmbed.addField(message.author.username + ' suggests:', text)
  suggestionEmbed.setFooter(message.author.username, message.author.avatarURL)
  message.guild.channels.find(c => c.name === 'suggestions').send(suggestionEmbed).catch(console.error).then(async suggestionEmbed => {
    await suggestionEmbed.react('👍')
    await suggestionEmbed.react('👎')
  })
}
function getLogs () {
  request.get('http://142.44.143.13:7345/admin.log', function (error, response, body) {
    var adminLogNewU = body
    adminLogNew = adminLogNewU.split('\n')
  })
  request.get('http://142.44.143.13:7345/surveilance.log', function (error, response, body) {
    var surveilanceLogNewU = body
    surveilanceLogNew = surveilanceLogNewU.split('\n')
  })
}

function postRepo (message) {
  var repoEmbed = new Discord.RichEmbed()
    .setColor(3447003)
    .setThumbnail('https://i.imgur.com/7hKPqeU.png')
    .setTimestamp()
    .addField('**Repository Information:**', '**Repo Link:** ftp://74.91.124.56/.a3s/autoconfig\n**Arma3Sync:** `www.armaholic.com/page.php?id=22199`')
    .setFooter('!repo')
  return message.channel.send(repoEmbed)
}
function postInformation1 (message) {
  if (message.member.roles.find(n => n.name === 'Developer')) {
    message.guild.channels.find(c => c.name === 'information').send({ files: ['banner.png'] })
  }
}
function postInformation2 (message) {
  if (message.member.roles.find(n => n.name === 'Developer')) {
    message.guild.channels.find(c => c.name === 'information').send('**Roleplay Network - Home of Roleplay and Entertainment!**\n\nWelcome to Silverlake and Metro! It has been our main goal to bring back what everyone once loved and constantly update it with amazing content for everyone to enjoy! \n\n:bell: You can turn on notifications for #announcements by typing the command !Notify in #bot-commands\n\n:pushpin: **Official Links**\n\n**Website:**  http://roleplay.network/\n**Teamspeak:** Connect using ts3.project-silverlake.com\n**REPO:** ftp://74.91.124.56/.a3s/autoconfig')
  }
}
// Handles message requests.
client.on('message', async (message) => {
  // Sets up a few dependencies for the message handleling.
  if (!message.content.startsWith(prefix)) return
  let unformatted = message.content.split(' ')[0]
  unformatted = unformatted.slice(prefix.length)
  let cmd = unformatted.toLowerCase()
  let args = message.content.split(' ').slice(1)
  // Checks the command and applies the correct function
  if (message.channel.name === 'bot-commands') {
    if (cmd === 'restart' || cmd === 'servers') {
      getServerDetails(message)
      message.delete(500).catch(O_o=>{});
    }
    if (cmd === 'announcement') {
      postAnnouncement(args, message)
      message.delete(500).catch(O_o=>{});
    }
    if (cmd === 'event') {
      postEvent(args, message)
      message.delete(500).catch(O_o=>{});
    }
    if (cmd === 'howto') {
      postHowto(message, args)
      message.delete(500).catch(O_o=>{});
    }
    if (cmd === 'help' || cmd === 'info') {
      postHelp(message)
      message.delete(500).catch(O_o=>{});
    }
    if (cmd === 'poll') {
      postPoll(message, args)
      message.delete(500).catch(O_o=>{});
    }
    if (cmd === 'changelog') {
      postChangelog(message)
      message.delete(500).catch(O_o=>{});
    }
    if (cmd === 'repo') {
      postRepo(message)
      message.delete(500).catch(O_o=>{});
    }
    if (cmd === 'suggest') {
      postSuggestion(message, args)
      message.delete(500).catch(O_o=>{});
    }
    if (cmd === 'bogg1') {
      postInformation1(message)
      message.delete(500).catch(O_o=>{});
      console.log('information posted')
    }
    if (cmd === 'bogg2') {
      postInformation2(message)
      message.delete(500).catch(O_o=>{});
      console.log('information posted')
    }
    if (cmd === 'announcement2') {
      var announcementEmbed = new Discord.RichEmbed()
      .setColor(3447003)
      .setThumbnail('https://i.imgur.com/7hKPqeU.png')
      .setTimestamp()
      .addField('A message to the community', 'Dear Community Members\n\nOver the past few weeks, our development team has been secretly working on a new project for the community. Because of this, it might have seemed like we abandoned Project Silverlake. However, we’ve been working hard on a second server for everyone to enjoy.\n\nSo what’s happening? Project Silverlake is being rebranded to Roleplay.Network, and we will now be hosting two seperate whitelisted servers. We will keep Project Silverlake up and running, and Max Kit will be taking over development for that server. Our second server will be Project Metro, and it will be managed by Cole.')
      .addField('-', '\n\nBoth servers will be whitelisted, and to be whitelisted you simply need to make a forum profile and go to staff. At that point a staff member will help you and will whitelist you for both servers. If you’re already whitelisted for Project Silverlake and you wish to play on Metropolis, go to the “Waiting for Whitelisting” channel and you will be whitelisted. \n\nAs part of the network transition we will be changing our teamspeak server. This will require everyone to get their tags again so please go to requesting tags with your profile. A staff member will then give you all your tags.\n\nAt this time, the Metropolis Server is ready for release, and we’re planning on doing a public launch on Friday, May 10th. The current repository is active with BOTH modpacks on the repo. Please keep in mind these are two completely separate modpacks, and they are not dependent on each other. You are to download either one depending on which server you wish to play. \n\nWe hope to see you all there on Friday!')
      message.guild.channels.find(c => c.name === 'announcements').send(announcementEmbed).catch(console.error)
    }
  } else {
    message.channel.send('Please post all bot commands in #bot-commands')
    message.delete(500).catch(O_o=>{});
  }
  if (startupCheck === true) {
    setInterval(() => {
      getLogs()
      client.user.setActivity("PL: " + PLarmaCurrentPlayers + " / " + PLarmaMaxPlayers + "M: " + MarmaCurrentPlayers + " / " + MarmaMaxPlayers, { type: 'WATCHING' })
    }, 300000)
    setInterval(() => {
      // Check and add any differences in the logs to the embed.
      if (!Array.isArray(adminLogOld)) {
        adminLogOld = adminLogNew
      }
      if (!Array.isArray(surveilanceLogOld)) {
        surveilanceLogOld = surveilanceLogNew
      }
      if (adminLogNew.length > adminLogOld.length || surveilanceLogNew.length > surveilanceLogOld.length) {
        if (adminLogNew.length > adminLogOld.length) {
          let differenceA = ''
          differenceA = adminLogNew.filter(x => !adminLogOld.includes(x))
          differenceA.join('\n')
          adminLogOld = adminLogNew
          if (differenceA.length > 0) {
            message.guild.channels.find(c => c.name === 'server-logs').send('**Admin Log:**').catch(console.error)
            message.guild.channels.find(c => c.name === 'server-logs').send(differenceA, { split: true }).catch(console.error)
          }
        }
        if (surveilanceLogNew.length > surveilanceLogOld.length) {
          let differenceS = ''
          differenceS = surveilanceLogNew.filter(x => !surveilanceLogOld.includes(x))
          differenceS.join('\n')
          surveilanceLogOld = surveilanceLogNew
          if (differenceS.length > 0) {
            message.guild.channels.find(c => c.name === 'server-logs').send('**Surveilance Log:**').catch(console.error)
            message.guild.channels.find(c => c.name === 'server-logs').send(differenceS, { split: true }).catch(console.error)
          }
        }
      }
    }, 305000)
    startupCheck = false
  }
})
